import re
import scrapy


years_pattern = re.compile('201\d')


class CrosswebWorkshopsSpider(scrapy.Spider):
    name = 'crossweb_workshops_spider'
    start_urls = [
        'http://crossweb.pl/archiwum/?miasto=krakow',
        'http://crossweb.pl/archiwum/?miasto=wroclaw',
        'http://crossweb.pl/archiwum/?miasto=katowice',
        'http://crossweb.pl/archiwum/?miasto=warszawa',
    ]

    def parse(self, response):
        for event in response.css('.brow'):
            meeting_type = event.css('.colTab.type::text').extract_first()
            day_month = event.css('.colDataDay::text').extract_first()
            day_month = day_month.strip().split('.') if day_month else (0, 0)
            year = event.css('.colData.old::text').extract_first().strip()
            event_data = {
                'year': int(year) if year else 0,
                'month': day_month[1],
                'day': day_month[0],
                'title': years_pattern.sub('', event.css('.colTab.title::text').extract_first()).strip(),
            }
            if meeting_type == 'Warsztaty' and event_data['year'] in range(2014, 2017):
                yield event_data
