from datetime import datetime, timedelta, time
from decimal import Decimal
from itertools import takewhile, dropwhile
import random
from time import sleep
import timeit

from names import full_names
from models import *
from utils import *


random.seed(10)
create_database('postgres')
# create_database()

YEARS = 3
CONFERENCES_A_MONTH = 2

AVERAGE_PEOPLE_ON_CONFERENCE = 200
AVERAGE_COMPANIES_ON_CONFERENCE = 8
AVERAGE_CONFERENCES_PER_COMPANY = 3
AVERAGE_CONFERENCES_PER_INDIVIDUAL = 4
AVERAGE_DAYS_PER_CONFERENCE = 2.5
AVERAGE_EMPLOYEES = 100
AVERAGE_EMPLOYEES_ON_CONFERENCE = 4
STUDENT_ID_RANGE = range(100000, 999999)
AVERAGE_CONFERENCE_DAY_MAX_PARTICIPANTS = 1000
AVERAGE_CONFERENCE_DAY_PRICE = 75.6
AVERAGE_CONFERENCE_DAY_TRESHOLDS = 2
AVERAGE_WORKSHOPS_PER_DAY = 4
AVERAGE_WORKSHOP_MAX_PARTICIPANTS = 1000
AVERAGE_WORKSHOP_PRICE = 40
MIN_DAYS_BEFORE_NORMAL_PRICE = 6

STUDENT_MULTIPLICATOR = Decimal(.49)
DAYS_FOR_PAYMENT = 7

DAY_START_HOUR = 9
DAY_END_HOUR = 17

START_DATE = datetime(2014, 1, 1)
END_DATE = datetime(2017, 1, 1)


def override_full_names(amount=None):
    if amount is None:
        while True:
            yield 'Marta', 'Adamczyk'
    else:
        for _ in range(amount):
            yield 'Adam', 'Nowicki'


def random_treshold():
    return int(abs(random.weibullvariate(35, 1)))


@log
@db_session
def generate_participants(amount=int(random.gauss(
        YEARS * 12 * CONFERENCES_A_MONTH * AVERAGE_PEOPLE_ON_CONFERENCE,
        YEARS * 12 * CONFERENCES_A_MONTH * AVERAGE_PEOPLE_ON_CONFERENCE / 4))):
    for firstname, lastname in full_names(amount):
        student_id = random.randint(STUDENT_ID_RANGE.start, STUDENT_ID_RANGE.stop)
        Participant(firstname=firstname,
                    lastname=lastname,
                    student_id=(student_id if not student_id % 6 else None))
    return Participant.select().count()


@log
@db_session
def generate_company_registrants(amount=abs(int(random.gauss(
        YEARS * 12 * CONFERENCES_A_MONTH * AVERAGE_COMPANIES_ON_CONFERENCE / AVERAGE_CONFERENCES_PER_COMPANY,
        YEARS * 12 * CONFERENCES_A_MONTH * AVERAGE_COMPANIES_ON_CONFERENCE / 4)))):
    for firstname, lastname in full_names(amount):
        companyname = gen_company_name(firstname, lastname)
        email_address = gen_email(firstname, lastname)
        phone_number = gen_phone_number()
        Registrant(company_name=companyname,
                   phone_number=(phone_number if phone_number[5] not in '39' else ''),
                   email=email_address,
                   first_name=firstname,
                   last_name=lastname)
    return Registrant.select().count()


@log
@db_session
def generate_conferences(amount=int(random.gauss(
        YEARS * 12 * CONFERENCES_A_MONTH,
        YEARS * 12 * CONFERENCES_A_MONTH / 4))):
    for title in conference_titles(amount):
        Conference(title=title)
    return Conference.select().count()


@log
@db_session
def generate_conference_days():
    difference = END_DATE - START_DATE
    conferences = Conference.select()
    starting_days = [int(random.random() * difference.days) for _ in conferences]
    for start, conference in zip(starting_days, conferences):
        days = abs(int(random.gauss(AVERAGE_DAYS_PER_CONFERENCE - 1, 1))) + 1
        for i in range(days):
            date = START_DATE + timedelta(days=start + i)
            event = Event(max_participants=int(random.gauss(AVERAGE_CONFERENCE_DAY_MAX_PARTICIPANTS,
                                                            AVERAGE_CONFERENCE_DAY_MAX_PARTICIPANTS / 4)))
            ConferenceDay(day=date,
                          conference=conference,
                          event=event)
    return ConferenceDay.select().count()


@log
@db_session
def generate_workshops():
    titles = workshop_titles()
    for cday in ConferenceDay.select():
        workshops = abs(int(random.gauss(AVERAGE_WORKSHOPS_PER_DAY, AVERAGE_WORKSHOPS_PER_DAY * 3 / 4)))
        starting_hours = [round(4 * random.uniform(DAY_START_HOUR, DAY_END_HOUR - 1.5)) / 4 for _ in range(workshops)]
        lengths = [1.5 if hour > 14 else random.choice([1.5, 3]) for hour in starting_hours]
        for start, length, title in zip(starting_hours, lengths, titles):
            start_time = time(hour=int(start), minute=int(60 * (start - int(start))))
            end_time = time(hour=int(start + length), minute=int(60 * (start + length - int(start + length))))
            event = Event(max_participants=int(random.gauss(AVERAGE_WORKSHOP_MAX_PARTICIPANTS,
                                                            AVERAGE_WORKSHOP_MAX_PARTICIPANTS / 4)))
            price = max(0, random.gauss(AVERAGE_WORKSHOP_PRICE / 2, AVERAGE_WORKSHOP_PRICE))
            price = 0 if price < 5 else round(price, 2)
            Workshop(start_time=start_time,
                     end_time=end_time,
                     conference_day=cday,
                     event=event,
                     title=title,
                     participant_price=price
                     )
    return Workshop.select().count()


@log
@db_session
def generate_prices():
    for cday in ConferenceDay.select():
        n_tresholds = max(0, int(random.gauss(AVERAGE_CONFERENCE_DAY_TRESHOLDS, AVERAGE_CONFERENCE_DAY_TRESHOLDS / 2)))
        tresholds = sorted(list({random_treshold() +
                                 MIN_DAYS_BEFORE_NORMAL_PRICE for _ in range(n_tresholds)}) + [0])
        max_treshold = tresholds[-1] if tresholds[-1] else 1
        base_price = random.gauss(AVERAGE_CONFERENCE_DAY_PRICE, AVERAGE_CONFERENCE_DAY_PRICE / 4)
        max_discount = random.uniform(base_price / 10, base_price / 3 * 2)
        prices = [round(base_price - (t / max_treshold) * max_discount, 2) for t in tresholds]

        for treshold, price in zip(tresholds, prices):
            c = ConferenceDayPrice(price=price if tresholds[-1] else 0,
                                   registration_date=cday.day - timedelta(days=treshold),
                                   conference_day=cday)
    return ConferenceDayPrice.select().count()


@db_session
def create_registrant(participant):
    return Registrant(first_name=participant.firstname,
                      last_name=participant.lastname,
                      email=gen_email(participant.firstname, participant.lastname))

@db_session
def register(reservation, event_confday_workshop, regular_people, students):
    event = event_confday_workshop.event
    currently_registered_for_event = select(sum(r.regular_slots + r.student_slots) for r in ReservationDetails if r.event == event)[:][0]
    if currently_registered_for_event + len(regular_people) + len(students) > event.max_participants:
        return None
    else:
        return ReservationDetails(
                           event=event,
                           reservation=reservation,
                           regular_slots=len(regular_people),
                           student_slots=len(students),
                           participants=regular_people+students)

@db_session
def get_or_create_reservation(conference, registrant, date_time):
    selected_reservation = select(r
                                  for r in Reservation
                                  if r.conference == conference
                                  and r.registrant == registrant).first()
    return selected_reservation or Reservation(conference=conference,
                                               registrant=registrant,
                                               to_pay=0,
                                               date_time=date_time,
                                               should_pay_before=date_time+timedelta(days=DAYS_FOR_PAYMENT))

@log
@db_session
def generate_company_reservations():
    company_registrants = select(r
                                 for r in Registrant
                                 if r.company_name)
    for registrant in company_registrants:
        employees_n = int(abs(random.gauss(AVERAGE_EMPLOYEES, AVERAGE_EMPLOYEES * 3 / 2)) + 1)
        company_participants = Participant.select_random(employees_n)

        for cday in ConferenceDay.select():
            slots_n = int(abs(random.gauss(AVERAGE_EMPLOYEES_ON_CONFERENCE,
                                           AVERAGE_EMPLOYEES_ON_CONFERENCE * 2 / 3)) + 1)
            participants = random.sample(company_participants, min(slots_n, len(company_participants)))
            date_time = shift_date(cday.day, random_treshold())
            reservation = get_or_create_reservation(cday.conference, registrant, date_time)
            students = list(filter(lambda p: p.student_id is not None,participants))
            regular_people = list(filter(lambda p: p.student_id is None,participants))
            register(reservation, cday, regular_people, students)

            for workshop in Workshop.select(lambda w: w.conference_day == cday):
                slots_n = int(abs(random.gauss(AVERAGE_EMPLOYEES_ON_CONFERENCE,
                                               AVERAGE_EMPLOYEES_ON_CONFERENCE * 2 / 3)) + 1)
                participants = random.sample(participants, min(slots_n, len(participants)))
                students = list(filter(lambda p: p.student_id is not None,participants))
                regular_people = list(filter(lambda p: p.student_id is None,participants))
                register(reservation, workshop, regular_people, students)

    return Reservation.select().count()


@log
@db_session
def generate_individual_reservations():
    individuals = select(p
                         for p in Participant
                         if len(p.reservations_details) is 0)
    n_created = 0
    for individual in individuals:
        n_conferences = abs(int(random.gauss(AVERAGE_CONFERENCES_PER_INDIVIDUAL,
                                             AVERAGE_CONFERENCES_PER_INDIVIDUAL / 2)))
        n_workshops = abs(int(random.gauss(AVERAGE_CONFERENCES_PER_INDIVIDUAL,
                                           AVERAGE_CONFERENCES_PER_INDIVIDUAL / 2)))
        registrant = create_registrant(individual)

        for cday in ConferenceDay.select_random(n_conferences):
            date_time = shift_date(cday.day, random_treshold())
            reservation = get_or_create_reservation(cday.conference, registrant, date_time)
            participants = ([], [individual]) if individual.student_id is not None else ([individual], [])
            if register(reservation, cday, *participants):
                n_created += 1

            for workshop in Workshop.select(lambda w: w.conference_day == cday).random(n_workshops):
                if register(reservation, workshop, *participants):
                    n_created += 1

    return n_created

def get_conference_day_cost(cday_prices, date_time):
    _date = date_time.date()
    try:
        return (list(dropwhile(
            lambda price: price.registration_date < _date,
            sorted(cday_prices, key=lambda price: price.registration_date)))[0]).price
    except IndexError:
        return 0


def event_cost(event, date_time):
    return event.workshop.participant_price if event.workshop is not None else\
           get_conference_day_cost(event.conference_day.conference_day_prices, date_time)


def compute_reservation_price(reservation):
    return sum((details.regular_slots + STUDENT_MULTIPLICATOR*details.student_slots) * event_cost(details.event, reservation.date_time)
               for details
               in reservation.reservations_details)


@log
@db_session
def generate_reservations_payments():
    reservations = Reservation.select()
    for reservation in reservations:
        price = compute_reservation_price(reservation)
        is_recent = reservation.date_time.date() > (datetime.now() - timedelta(days=31)).date()
        reservation.to_pay = price
        reservation.paid = price if not is_recent else (
                           0 if is_recent and int(price) % 6 is 0 else \
                           price * Decimal(random.choice([
                               10,
                               .1,
                               random.random(),
                               ] +
                               [1]*6)))

    return reservations.count()


def make_students_unique():
    """This should be done after seeding database with students to ensure
    the student_ids are distinct:
    update participant
    set student_id = (student_id+1)
    where id in
	(select distinct on (student_id) id from participant
	where student_id in
		(select student_id from
		participant
		group by student_id
		having count(student_id) > 1)
	order by student_id)
    """
    pass


def populate_database():
    generate_participants()
    generate_company_registrants()
    generate_conferences()
    generate_conference_days()
    generate_workshops()
    generate_prices()
    generate_company_reservations()
    generate_individual_reservations()
    generate_reservations_payments()


def clear_database():
    db.drop_all_tables(with_all_data=True)
    db.create_tables()


def main():
    clear_database()
    populate_database()


if __name__ == '__main__':
    # main()
    print("-" * 55 + "\n" +
          "Generation took {:.2f}s".format(timeit.timeit('main()', number=1, setup='from __main__ import main')))
