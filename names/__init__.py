from __future__ import unicode_literals
from builtins import zip
from bisect import bisect_left
from itertools import repeat
from os.path import abspath, join, dirname
import random
from shlex import split


try:
    # python3
    from functools import lru_cache
except ImportError:
    # python2
    from repoze.lru import lru_cache


__title__ = 'names'
__version__ = '0.3.0.post1'
__author__ = 'Trey Hunner'
__license__ = 'MIT'


def full_path(filename):
    return abspath(join(dirname(__file__), filename))


FILES = {
    'first:male': full_path('dist.male.first'),
    'first:female': full_path('dist.female.first'),
    'last': full_path('dist.all.last'),
}


@lru_cache(len(FILES))
def get_names(filename):
    names = []
    with open(filename) as name_file:
        names = [(name, float(cummulative))
                 for (name, _, cummulative, _) in map(split, name_file)]
    return names


def get_name(filename):
    selected = random.random() * 90
    names = get_names(filename)
    cummulatives = [pair[1] for pair in names]
    key = bisect_left(cummulatives, selected)
    return names[key][0] if len(names) else ""


def get_first_name(gender=None):
    if gender is None:
        gender = random.choice(('male', 'female'))
    if gender not in ('male', 'female'):
        raise ValueError("Only 'male' and 'female' are supported as gender")
    return get_name(FILES['first:%s' % gender]).capitalize()


def get_last_name():
    return get_name(FILES['last']).capitalize()


def get_full_name(gender=None):
    return "{0} {1}".format(get_first_name(gender), get_last_name())


def first_names(times=None, gender=None):
    counter = repeat(None) if not times else repeat(None, times)
    for _ in counter:
        yield get_first_name(gender)


def last_names(times=None):
    counter = repeat(None) if not times else repeat(None, times)
    for _ in counter:
        yield get_last_name()


def full_names(times=None, gender=None):
    for first, last in zip(first_names(times, gender), last_names(times)):
        yield first, last
