from datetime import datetime, timedelta
from collections import namedtuple
import csv
import random

import phonenumbers as pns


DATA_DIRECTORY = "data"
CONFERENCES_FILE = "confs.csv"
WORKSHOPS_FILE = "workshops.csv"
DEFAULT_PHONE_COUNTRY = "PL"

workshops = dict()
with open("{}/{}".format(DATA_DIRECTORY, WORKSHOPS_FILE)) as workshops_csv:
    WorkshopData = namedtuple("WorkshopData", next(workshops_csv))
    workshops_reader = csv.reader(workshops_csv)
    for conf in workshops_reader:
        data = WorkshopData(*conf)
        workshops[data.title] = data

conferences = dict()
with open("{}/{}".format(DATA_DIRECTORY, CONFERENCES_FILE)) as conferences_csv:
    ConferenceData = namedtuple("ConferenceData", next(conferences_csv))
    conferences_reader = csv.reader(conferences_csv)
    for conf in conferences_reader:
        data = ConferenceData(*conf)
        conferences[data.title] = data


def log(fun):
    def wrapper(*args, **kwargs):
        print("Generating {}...".format(fun.__name__.replace('generate_', '', 1).replace('_', ' ')), end=' ', flush=True)
        print("OK: generated {}".format(fun(*args, **kwargs)))
    return wrapper


def gen_company_name(first, last):
    suffixes = ["S.A.", "sp. z o.o.", "sp. j.", "sp.p.", "sp. k.", "S.K.A."]
    return "{}{} {}".format(first[:3].capitalize(), last[-4:].lower(), random.choice(suffixes))


def gen_email(first, last):
    domains = ["gmail.com", "wp.pl", "hotmail.com", "outlook.com"]
    return "{}.{}@{}".format(first.lower(), last.lower(), random.choice(domains))


def gen_phone_number(number=None):
    if number is None:
        number = random.randint(5 * 10e7, 9 * 10e7)
    number = pns.parse(str(number), "PL")
    return pns.format_number(number, pns.PhoneNumberFormat.INTERNATIONAL)


def conference_titles(limit=None):
    for title, _ in zip(conferences, range(limit or len(conferences))):
        yield title


def workshop_titles(limit=None):
    for title, i in zip(workshops, range(limit or len(workshops))):
        yield title


def shift_date(date, treshold):
    return datetime.combine(date, datetime.min.time()) - timedelta(days=treshold,
                                                                   hours=random.randint(0, 24),
                                                                   minutes=random.randint(0, 60))
