from datetime import date, datetime, time
from decimal import Decimal
from pony.orm import *


db = Database()


class Conference(db.Entity):
    id = PrimaryKey(int, auto=True)
    conference_days = Set('ConferenceDay')
    title = Required(str)
    reservations = Set('Reservation')


class Participant(db.Entity):
    id = PrimaryKey(int, auto=True)
    firstname = Required(str)
    lastname = Required(str)
    student_id = Optional(int)
    reservations_details = Set('ReservationDetails')


class Registrant(db.Entity):
    id = PrimaryKey(int, auto=True)
    company_name = Optional(str, nullable=True)
    reservations = Set('Reservation')
    phone_number = Optional(str, nullable=True)
    email = Required(str)
    first_name = Required(str)
    last_name = Required(str)


class ConferenceDay(db.Entity):
    id = PrimaryKey(int, auto=True)
    conference_day_prices = Set('ConferenceDayPrice')
    workshops = Set('Workshop')
    conference = Required(Conference)
    day = Required(date)
    event = Required('Event')


class Workshop(db.Entity):
    id = PrimaryKey(int, auto=True)
    conference_day = Required(ConferenceDay)
    start_time = Required(time)
    end_time = Required(time)
    title = Required(str)
    event = Required('Event')
    participant_price = Required(Decimal)


class Event(db.Entity):
    id = PrimaryKey(int, auto=True)
    conference_day = Optional(ConferenceDay)
    workshop = Optional(Workshop)
    max_participants = Required(int)
    reservations_details = Set('ReservationDetails')


class Reservation(db.Entity):
    id = PrimaryKey(int, auto=True)
    conference = Required(Conference)
    registrant = Required(Registrant)
    date_time = Required(datetime)
    paid = Required(Decimal, default=0)
    reservations_details = Set('ReservationDetails')
    should_pay_before = Required(datetime)
    to_pay = Required(Decimal)


class ConferenceDayPrice(db.Entity):
    id = PrimaryKey(int, auto=True)
    price = Required(Decimal, default=0)
    registration_date = Required(date)
    conference_day = Required(ConferenceDay)


class ReservationDetails(db.Entity):
    id = PrimaryKey(int, auto=True)
    reservation = Required(Reservation)
    event = Required(Event)
    participants = Set(Participant)
    regular_slots = Required(int)
    student_slots = Required(int, default=0)


def create_database(connection_type='sqlite'):
    if connection_type == 'sqlite':
        filename = 'database.sqlite'
        db.bind("sqlite", filename, create_db=True)
    elif connection_type =='postgres':
        db.bind("postgres", user="", password="", host="dbadmin.iisg.agh.edu.pl", database="")
    else:
        raise ValueError('Connection types other than {} are not supported'.format(",".join(['sqlite', 'postgres'])))
    db.generate_mapping(create_tables=True)
    commit()
